package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Deal struct {
	Ticker string  `json:"ticker"`
	Price  float64 `json:"price"`
	Ts     int64   `json:"ts"`
}

type Candle struct {
	Ticker  string  `json:"ticker"`
	Ts      int64   `json:"ts"`
	open_p  float64 `json:"open_p"`
	max     float64 `json:"max"`
	min     float64 `json:"min"`
	close_p float64 `json:"close_p"`
}

type TimeSorter []Candle

func (a TimeSorter) Len() int           { return len(a) }
func (a TimeSorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a TimeSorter) Less(i, j int) bool { return a[i].Ts < a[j].Ts }

func max_num(arr []float64) float64 {
	max := 0.0
	for _, n := range arr {
		max = float64(math.Max(float64(max), float64(n)))
	}
	return max
}

func min_num(arr []float64) float64 {
	min := 1000000.0
	for _, n := range arr {
		min = float64(math.Min(float64(min), float64(n)))
	}
	return min
}

func time_parse(t string) int64 {
	layout := "2006-01-02T15:04:05.000000"
	str := strings.Replace(t, " ", "T", -1)
	ts, err := time.Parse(layout, str)
	if err != nil {
		//panic(err)
		fmt.Println(err)
	}
	if (ts.Hour() >= 7) && (ts.Hour() <= 23) {
		return ts.Unix()
	} else {
		return 0
	}
}

func candle_sort(data []Deal, delta int64) []Candle {
	var candles []Candle
	var t0 int64 = 1548831600 // 1548831600000000000 //GMT 30.01.19 7:00
	var prices []float64
	for _, deal := range data {
		if t0 <= deal.Ts && deal.Ts < t0+delta {
			prices = append(prices, deal.Price)

		} else if deal.Ts >= t0+delta && len(prices) != 0 {
			candles = append(candles, Candle{
				Ticker:  deal.Ticker,
				Ts:      t0 - 10800, //time.Unix(t0-10800, 0),
				open_p:  prices[0],
				max:     max_num(prices),
				min:     min_num(prices),
				close_p: prices[len(prices)-1],
			})
			prices = nil
			t0 = deal.Ts - (deal.Ts % delta)
		} else {
			prices = nil
			t0 = t0 + delta
		}
	}

	return candles
}

func csv_filling(in []Candle, delta int64) {

	name := strconv.Itoa(int(delta)) + "_min.csv"
	file, err := os.Create(name)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range in {
		result := []string{value.Ticker, time.Unix(value.Ts, 0).Format(time.RFC3339),
			strconv.FormatFloat(value.open_p, 'f', 2, 64),
			strconv.FormatFloat(value.max, 'f', 2, 64),
			strconv.FormatFloat(value.min, 'f', 2, 64),
			strconv.FormatFloat(value.close_p, 'f', 2, 64),
		}
		writer.Write(result)
	}

}

func get_candles(delta int64) {
	csvFile, _ := os.Open("trades.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var apl []Deal
	var amz []Deal
	var sbr []Deal
	var candles []Candle

	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}

		switch ticker := line[0]; ticker {
		case "AAPL":
			ts := time_parse(line[3])
			price, err := strconv.ParseFloat(line[1], 64)
			if err != nil {
				log.Fatal(err)
			} else if ts != 0 {
				apl = append(apl, Deal{
					Ticker: line[0],
					Price:  price,
					Ts:     ts,
				})
			}
		case "AMZN":
			ts := time_parse(line[3])
			price, err := strconv.ParseFloat(line[1], 64)
			if err != nil {
				log.Fatal(err)
			} else if ts != 0 {
				amz = append(amz, Deal{
					Ticker: line[0],
					Price:  price,
					Ts:     ts,
				})
			}
		case "SBER":
			ts := time_parse(line[3])
			price, err := strconv.ParseFloat(line[1], 64)
			if err != nil {
				log.Fatal(err)
			} else if ts != 0 {
				sbr = append(sbr, Deal{
					Ticker: line[0],
					Price:  price,
					Ts:     ts,
				})
			}
		}
	}
	sbr_candles := candle_sort(sbr, delta*60)
	amz_candles := candle_sort(amz, delta*60)
	apl_candles := candle_sort(apl, delta*60)

	candles = append(sbr_candles, apl_candles...)
	candles = append(candles, amz_candles...)
	sort.Sort(TimeSorter(candles))

	csv_filling(candles, delta)

}

func main() {
	get_candles(5)
	get_candles(30)
	get_candles(4 * 60)
}
